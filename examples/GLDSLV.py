#!/usr/bin/python
# -*- coding: utf-8 -*-
# athena.execution.simulated_execution.py
'''
@since: 2014-11-12
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: Simple historical backtesting of crossing moving average
          strategy, with data from the SQLite database.
'''
# General imports
import datetime
# Athena Imports
from backtest import backtest
from data import sqlite_data
from events import events
from execution import simulated_execution
import numpy as np
from portfolio import portfolio
from strategy import strategy


# pylint: disable=too-few-public-methods

class MovingAverageCrossStrategy(strategy.Strategy):

    """
    Carries out a basic Moving Average Crossover strategy with a
    short/long simple weighted moving average. Default short/long
    windows are 50/200 periods respectively.
    """

    def __init__(self, bars, events, short_window=50, long_window=200):
        '''
        Initializes the buy and hold strategy.

        :param bars: The DataHandler object that provides bar information
        :param events: he Event Queue object.
        :param short_window: The short moving average lookback.
        :param long_window: The long moving average lookback.
        '''

        self.bars = bars
        self.symbol_list = self.bars.symbol_list
        self.events = events
        self.short_window = short_window
        self.long_window = long_window

        # Set to True if a symbol is in the market
        self.bought = self._calculate_initial_bought()

    def _calculate_initial_bought(self):
        """
        Adds keys to the bought dictionary for all symbols
        and sets them to 'OUT'.
        """
        bought = {}
        for symbol in self.symbol_list:
            bought[symbol] = 'OUT'
        return bought

    def calculate_signals(self, event):
        """
        Generates a new set of signals based on the MAC
        SMA with the short window crossing the long window
        meaning a long entry and vice versa for a short entry.

        :param event - A MarketEvent object.

        :return signal object
        """
        if event.type == 'MARKET':
            for symbol in self.symbol_list:
                bars = self.bars.get_latest_bars_values(
                    symbol, "adj_close", bars=self.long_window)

                if bars is not None and bars != []:
                    short_sma = np.mean(bars[-self.short_window:])
                    long_sma = np.mean(bars[-self.long_window:])

                    dt = self.bars.get_latest_bar_datetime(symbol)
                    sig_dir = ""
                    strength = 1.0
                    strategy_id = 1

                    if short_sma > long_sma and self.bought[symbol] == "OUT":
                        sig_dir = 'LONG'
                        signal = events.SignalEvent(
                            strategy_id, symbol, dt, sig_dir, strength)
                        self.events.put(signal)
                        self.bought[symbol] = 'LONG'

                    elif short_sma < long_sma and self.bought[symbol] == "LONG":
                        sig_dir = 'EXIT'
                        signal = events.SignalEvent(
                            strategy_id, symbol, dt, sig_dir, strength)
                        self.events.put(signal)
                        self.bought[symbol] = 'OUT'


def main():
    WORKING_ON = 'PC'
    if WORKING_ON == 'PC':
        source_dir = 'C:/Users/javgar119/Documents/Dropbox/SEC_MASTER/securities_master.sqlite'
    elif WORKING_ON == 'MAC':
        source_dir = '//Users/Javier/Dropbox/SEC_MASTER/securities_master.sqlite'

    #symbol_list = ['GOOG_YH']
    symbol_list = ['USDCOP_CSV']
    initial_capital = 100000.0
    start_date = datetime.datetime(1980, 12, 12, 0, 0, 0)
    heartbeat = 0.0

    my_backtest = backtest.Backtest(source_dir,
                                    symbol_list,
                                    initial_capital,
                                    heartbeat,
                                    start_date,
                                    sqlite_data.HistoricSQLiteDataHandler,
                                    simulated_execution.SimulatedExecutionHandler,
                                    portfolio.Portfolio,
                                    MovingAverageCrossStrategy)

    my_backtest.simulate_trading(graph_results=True)


if __name__ == "__main__":

    main()

    #import cProfile
    # cProfile.run('main()')
