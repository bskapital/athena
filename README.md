###############################################################################
# ATHENA
###############################################################################

Event driven trading and back-tester engine developed in Python 3

State: Early development, not suitable for use.


Roadmap:
 
-First stage: historical event-driven backtester, read form CSV
-Second stage: live paper trading with full reporting 
-Third stage: live trading

Let see how I'll manage to add complex event processing within all this.



===============================================================================
All commentaries, questions and constructive critics are welcome. 

I'm new programming and new to Python so I'm more than willing to take advise
from bright participants. javier.macro.trader@gmail.com