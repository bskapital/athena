#!/usr/bin/python
# -*- coding: utf-8 -*-
# reportsforhumans.report2sms.py
'''
@since: 2014-12-03
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary:

'''
# Official Google API for Python2
# https://github.com/google/google-api-python-client

# Unofficial API for Python3
# https://github.com/enorvelle/GoogleApiPython3x


import argparse
from httplib2 import Http
from oauth2client import tools
#from oauth2client.tools import run_flow
from oauth2client.client import OAuth2WebServerFlow
from apiclient import discovery
from oauth2client.file import Storage


#from dateutil import parser
import datetime as dt


def send2calendar(client_id,client_secret):
    # First we set up the authentication flow with Google
    # The credentials below are copied from the Google Developer Console

    scope = 'https://www.googleapis.com/auth/calendar'
    # This initiates the flow
    flow = OAuth2WebServerFlow(client_id, client_secret, scope)
    parser = argparse.ArgumentParser(parents=[tools.argparser])
    flags = parser.parse_args()
    # Previously stored credentials are retrieved
    # If credentials don't exist or are invalid new ones are obtained and
    # stored
    storage = Storage('credentials.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        # This will open a new window in your browser
        # You will have to confirm the authorization the first time
        credentials = tools.run_flow(flow, storage, flags)
    # Creates a service object and authorizes it with the credentials
    # The service object will be used to interact with the API
    http = Http()
    http = credentials.authorize(http)
    service = discovery.build(serviceName='calendar', version='v3', http=http)
    # Next we create a google calendar event
    # The start of the event is now, and the end 1 minute later
    start = dt.datetime.now()
    end = start + dt.timedelta(minutes=1)
    # The datetime objects are converted to strings
    start = dt.datetime.strftime(start, '%Y-%m-%dT%H:%M:%S')
    end = dt.datetime.strftime(end, '%Y-%m-%dT%H:%M:%S')
    # The calendar event is defined
    event = {
        # Start and end dates/times are passed on
        'start': {
            'dateTime': start,
            'timeZone': 'Asia/Bangkok'
        },
        'end': {
            'dateTime': end,
            'timeZone': 'Asia/Bangkok'
        },
        # The message to be sent is entered here (60 characters max)
        'summary': 'test',
        # This tells Google Calendar to send an sms to the registered phone
        # number
        'reminders': {
            'overrides':
            [
                {
                    'method': 'sms',
                    'minutes': 0,
                },
            ],
            'useDefault': False
        },
    }

    # The event is then created
    created_event = service.events().insert(calendarId='primary'
                                            ,body=event).execute()
    return created_event

if __name__ == '__main__':
    client_id = '223686409444-ivc1i68vng1i8ba2f1d7r6cbq69547n8.apps.googleusercontent.com'
    client_secret = 'NracRiA7z7ACecS2jN7FKtSH'
    ans = send2calendar(client_id,client_secret)
    print(ans)
