#!/usr/bin/python
# -*- coding: utf-8 -*-
# reportsforhumans.report2mail.py
'''
@since: 2014-12-03
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: 

'''


import smtplib


def send_email(server, from_address, to_address, user_pwr, subject, body):
    '''
    Send an email from Gmail or Zoho mail.

    :param server: "gmail" or "zoho"
    :param from_address: the address from where the email will be sent.
    :param to_address: the address to where the email will be sent
    :param user_pwr: the user password or application password for from_address
    :param subject:
    :param body:
    '''

    # Prepare actual message
    message = "From: {}\nTo: {}\nSubject: {}\n\n{}".format(
        from_address, to_address, subject, body)
    try:
        #server = smtplib.SMTP(SERVER)
        # or port 465 doesn't seem to work!
        if server == 'gmail':
            server = smtplib.SMTP("smtp.gmail.com", 587)
        if server == 'zoho':
            server = smtplib.SMTP("smtp.zoho.com", 587)
        server.ehlo()
        server.starttls()
        server.login(from_address, user_pwr)
        ans = server.sendmail(from_address, to_address, message)
        print(ans)
        server.close()
        print('successfully sent the mail')
    except:
        print("failed to send mail")

if __name__ == '__main__':
    gmail_user = 'destinatary'
    gmail_pwd = 'secret'
    zoho_user = 'zoho_user'
    zoho_pwr = 'secret'

    subject = "Test sending mail"
    body = "Body of message"

    send_email(server='zoho',
               from_address=zoho_user,
               to_address=gmail_user,
               user_pwr=zoho_pwr,
               subject=subject,
               body=body)
