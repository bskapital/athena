#!/usr/bin/python
# -*- coding: utf-8 -*-
# testcases.performance_test.py
'''
@since: 2014-11-25
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: Tests for the performance module

'''
import unittest
import pandas as pd
import numpy as np
from performance import performance
import math

# pylint: disable=too-many-public-methods
# Nothing I can do here, are Pylint methods


class TestPerformance(unittest.TestCase):

    """
    Test the performance measure
    """

    def test_create_drowdown1(self):
        """
        Test1: non random serie with 3 inflection points
        """
        rows = 100
        step = 1
        values = [100]

        inflection1 = 0.15
        inflection2 = 0.50
        inflection3 = 0.90

        for each_row in range(rows - 1):
            if each_row < (rows * inflection1):
                values.append(values[-1] - step)
            elif (each_row >= rows) * inflection1 and \
                    (each_row < rows) * inflection2:
                values.append(values[-1] + step)
            elif (each_row >= rows * inflection2) and \
                    (each_row < rows) * inflection3:
                values.append(values[-1] - step)
            elif each_row >= rows * inflection3:
                values.append(values[-1] + step)

        pnl = pd.Series(values, index=np.arange(rows))
        result = performance.create_drawdowns(pnl)
        expected = (40.0, 49.0)
        self.assertEqual(result, expected, 'Drawdown error calculation')

    def test_create_sharpe_ratio0(self):
        """
        test for correct calculations
        """
        simm = [10, 9, 11, 10, 12, 11, 13, 12, 14, 13, 15]
        prices = pd.Series(simm)
        returns = prices.pct_change()
        result = performance.create_sharpe_ratio(returns)
        expected = 5.85973697
        self.assertAlmostEqual(result, expected)

    def test_create_sharpe_ratio1(self):
        """
        test for non volatility serie
        """
        simm = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        prices = pd.Series(simm)
        returns = prices.pct_change()
        result = performance.create_sharpe_ratio(returns)

        self.assertTrue(math.isnan(result))
