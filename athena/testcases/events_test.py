#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 14/08/2014

@author: Javier Garcia, javier.macro.trader@gmail.com
'''

# pylint: disable=R0904


import datetime
import unittest

from events import events


class TestMarketEvents(unittest.TestCase):

    """
    testing of events.MarketEvent
    """

    def test_marketevent_type(self):
        """
        MarketEvent must return correct type
        """
        market_event = events.MarketEvent()
        self.assertEqual(market_event.type, 'MARKET',
                         'market not returning correct type')


class TestSignalEvent(unittest.TestCase):

    """
    testing of events.SignalEvent
    """

    def setUp(self):
        strategy_id = 1
        symbol = 'FAKE'
        date = datetime.datetime(2014, 1, 1, 0, 0, 0)
        signal_type = 'LONG'
        strength = 2
        self.signal_event = events.SignalEvent(strategy_id, symbol, date,
                                               signal_type, strength)

    def test_signalevent_type(self):
        """
        SignalEvent must return correct type
        """
        self.assertEqual(self.signal_event.type, 'SIGNAL',
                         'signal not returning correct type')


class TestOrderEvent(unittest.TestCase):

    """
    testing of events.OrderEvent
    """

    def setUp(self):
        symbol = 'FAKE'
        order_type = 'MKT'
        quantity = 100
        direction = 'BUY'
        self.order_event = events.OrderEvent(symbol, order_type,
                                             quantity, direction)

    def test_orderevent_type(self):
        """
        OrderEvent must return correct type
        """
        self.assertEqual(self.order_event.type, 'ORDER',
                         'order not returning correct type')

    def test_print_correct_order(self):
        """
        OrderEvent print correct order
        """
        expected = 'Order: Symbol:FAKE, Type:MKT, Quantity:100, Direction:BUY'
        result = self.order_event.print_order()
        self.assertEqual(result, expected, 'Order printed incorrectly')


class TestFillEvent(unittest.TestCase):

    """
    testing of events.FillEvent
    """

    def setUp(self):
        self.timeindex = datetime.datetime(2014, 1, 1, 18, 30, 56)
        self.symbol = 'FAKE'
        self.exchange = 'NYSE'
        self.direction = 'BUY'

    def test_fixedib_comission_usstocks(self):
        """
        Fixed IB commission
        """
        # case 1: min value $1.00
        quantity = 100
        fill_cost = quantity * 25
        fill_event = events.FillEvent(self.timeindex, self.symbol,
                                      self.exchange, quantity,
                                      self.direction, fill_cost)
        result = fill_event.calc_fixed_ib_commission_us_stocks()
        self.assertEqual(result, 1, 'IB fixed commision error 1')

        # case 2: max value 0.05% of trade value
        quantity = 1000
        fill_cost = quantity * 25
        fill_event = events.FillEvent(self.timeindex, self.symbol,
                                      self.exchange, quantity,
                                      self.direction, fill_cost)
        result = fill_event.calc_fixed_ib_commission_us_stocks()
        self.assertEqual(result, 5, 'IB fixed commision error 2')

        # case 3: fixed_per_share = 0.005
        quantity = 1000
        fill_cost = quantity * 0.25
        fill_event = events.FillEvent(self.timeindex, self.symbol,
                                      self.exchange, quantity,
                                      self.direction, fill_cost)
        result = fill_event.calc_fixed_ib_commission_us_stocks()
        self.assertEqual(result, 1.25, 'IB fixed commision error 3')
