#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 14/08/2014

@author: Javier Garcia,
@email: javier.macro.trader at gmail.com
'''

# pylint: disable=too-many-public-methods
# pylint think unittest has too many public methods

# pylint: disable=invalid-name


import unittest
import datetime
import os
from tempfile import gettempdir
from math import isnan
import queue
import numpy as np


from data.csv_data import HistoricCSVDataHandler
from utils.helpers import make_fake_csv


class TestOpenConvertCSVfiles(unittest.TestCase):

    """
    Testing of method _open_convert_csv_files
    """

    def setUp(self):
        """
        create two fake CSV files using different start date
        to test correct reindex, also create market event.
        """
        self.csv_dir = gettempdir()
        self.symbol_list = ['FakeData1', 'FakeData2']
        self.start_dates = ['2014-05-05 17:30:00', '2014-05-10 17:30:00']
        self.end_dates = ['2014-05-20 17:30:00', '2014-05-15 17:30:00']

        for index, symbol in enumerate(self.symbol_list):
            make_fake_csv(save_to=self.csv_dir,
                          filename=symbol,
                          start_date=self.start_dates[index],
                          end_date=self.end_dates[index],
                          style=None)

        self.event = 'Market'
        self.tested_handler = HistoricCSVDataHandler(events=self.event,
                                                     csv_dir=self.csv_dir,
                                                     symbol_list=self.
                                                     symbol_list)

    def test_sorted_correctly(self):
        """
        _Open_Convert_CSV_files each dataframe correctly sorted.
        """
        for symbol in self.symbol_list:
            df_symbol = self.tested_handler.all_data_dic[symbol]
            self.assertTrue(df_symbol.index.is_monotonic,
                            'index is not monotonic')

    def test_start_index_correct(self):
        """
        _Open_Convert_CSV_file All dataframes start at the correct date
        """
        for symbol in self.symbol_list:
            df_symbol = self.tested_handler.all_data_dic[symbol]
            expected = datetime.datetime.strptime(self.start_dates[0],
                                                  "%Y-%m-%d %H:%M:%S")
            self.assertEqual(expected, df_symbol.index[0])

    def test_end_index_correct(self):
        """
        _Open_Convert_CSV_file All dataframes end at the correct date
        """

        for symbol in self.symbol_list:
            df_symbol = self.tested_handler.all_data_dic[symbol]
            expected = datetime.datetime.strptime(self.end_dates[0],
                                                  "%Y-%m-%d %H:%M:%S")
            self.assertEqual(expected, df_symbol.index[-1])

    def test_filling_holes_beginning(self):
        """
        _Open_Convert_CSV_file filling holes at beginning with NaN
        """
        # fakedata2 shorter serie, after reindex must be contain in index of
        # fakedata1. Filling the holes with 'NaN'
        fakedata2 = self.tested_handler.all_data_dic['FakeData2']
        # first 5 rows are NaN
        for index_row in range(4):
            self.assertTrue(isnan(fakedata2.iloc[index_row][0]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][1]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][2]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][3]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][4]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][5]))

    def test_filling_holes_in_between(self):
        """
        _Open_Convert_CSV_file filling in between with random numbers
        """
        # fakedata2 shorter serie, after reindex must be contain in index of
        # fakedata1. Filling the holes with 'NaN'
        fakedata2 = self.tested_handler.all_data_dic['FakeData2']
        # last 5 rows are NaN
        for index_row in range(5, 11):
            self.assertTrue(isinstance(fakedata2.iloc[index_row][0], float))
            self.assertTrue(isinstance(fakedata2.iloc[index_row][1], float))
            self.assertTrue(isinstance(fakedata2.iloc[index_row][2], float))
            self.assertTrue(isinstance(fakedata2.iloc[index_row][3], float))
            self.assertTrue(isinstance(fakedata2.iloc[index_row][4], float))
            self.assertTrue(isinstance(fakedata2.iloc[index_row][5], float))

    def test_filling_holes_ending(self):
        """
        _Open_Convert_CSV_file filling holes at end with NaN
        """
        # fakedata2 shorter serie, after reindex must be contain in index of
        # fakedata1. Filling the holes with 'NaN'
        fakedata2 = self.tested_handler.all_data_dic['FakeData2']
        # last 5 rows are NaN
        for index_row in range(11, 15):
            self.assertTrue(isnan(fakedata2.iloc[index_row][0]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][1]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][2]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][3]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][4]))
            self.assertTrue(isnan(fakedata2.iloc[index_row][5]))

    def test_columns_correct_all_df(self):
        """
        _Open_Convert_CSV_file Columns are correct
        """
        headers = ['open', 'high', 'low', 'close', 'volume', 'adj_close']
        for symbol in self.symbol_list:
            df_symbol = self.tested_handler.all_data_dic[symbol]
            for index, col in enumerate(headers):
                self.assertEqual(df_symbol.columns[index], col)

    def tearDown(self):
        """
        remove the temp files
        """
        for symbol in self.symbol_list:
            filename = os.path.join(self.csv_dir, (symbol + '.csv'))
            try:
                # if error there is no file, if no file how the test run?
                os.remove(filename)
            except OSError:
                pass


class TestHistoricCSVDataHandler(unittest.TestCase):

    """
    Testing HistoricCSVDataHandler
    """

    def setUp(self):
        """
        create two fake CSV files
        """
        self.csv_dir = gettempdir()
        self.symbol_list = ['FakeData1', 'FakeData2']
        self.start_dates = ['2014-05-05 17:30:00', '2014-05-10 17:30:00']
        self.end_dates = ['2014-05-20 17:30:00', '2014-05-15 17:30:00']

        for index, symbol in enumerate(self.symbol_list):
            make_fake_csv(save_to=self.csv_dir,
                          filename=symbol,
                          start_date=self.start_dates[index],
                          end_date=self.end_dates[index],
                          style='consecutive')

        self.event = queue.Queue()
        self.tested_handler = HistoricCSVDataHandler(events=self.event,
                                                     csv_dir=self.csv_dir,
                                                     symbol_list=self.
                                                     symbol_list)

    def tearDown(self):
        """
        remove the temp files
        """
        for symbol in self.symbol_list:
            filename = os.path.join(self.csv_dir, (symbol + '.csv'))
            try:
                # if error there is no file, if no file how the test run?
                os.remove(filename)
            except OSError:
                pass

    def test_get_latest_bar_not_initialized(self):
        """
        get_latest_bar Raise KeyError if latest_symbol_data not initialized.
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bar(self.symbol_list[0])

    def test_get_latest_bar_not_symbol(self):
        """
        get_latest_bar Raise KeyError if symbol not in database
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bar('ErrorSymbol')

    def test_get_latest_bar_correct_value(self):
        """
        get_latest_bar normal case multiple calls
        """
        for row in range(15):
            self.tested_handler.update_bars()
            result = self.tested_handler.get_latest_bar(self.symbol_list[0])
            self.assertEqual(row, result[1][0],
                             'normal case error, col=0')
            self.assertEqual(row + 0.2, result[1][2],
                             'normal case error, col=2')

    def test_get_latest_bars_not_initialized(self):
        """
        get_latest_bars Raise KeyError if latest_symbol_data not initialized.
        """
        bars = 2
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bars(self.symbol_list[0], bars)

    def test_get_latest_bars_not_symbol(self):
        """
        get_latest_bars Raise KeyError if symbol not in database
        """
        bars = 2
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bars('ErrorSymbol', bars)

    def test_get_latest_bars_correct_value(self):
        """
        get_latest_bars normal case
        """
        bars = 3
        for dummy in range(bars):
            self.tested_handler.update_bars()

        tmp = self.tested_handler.get_latest_bars('FakeData1', bars)

        expected = [0.0, 1.0, 2.0]
        result = [tmp[0][1][0], tmp[1][1][0], tmp[2][1][0]]
        self.assertListEqual(expected, result)

        expected = [0.2, 1.2, 2.2]
        tmp = [tmp[0][1][2], tmp[1][1][2], tmp[2][1][2]]
        self.assertListEqual(expected, tmp)

    def test_get_latest_bar_datetime_not_initialized(self):
        """
        get_latest_bar_datetime not_initialized
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bar_datetime(self.symbol_list[1])

    def test_get_latest_bar_datetime_not_symbol(self):
        """
        get_latest_bar_datetime if symbol not in database
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bar_datetime('ErrorSymbol')

    def test_get_latest_bar_datetime_correct_values(self):
        """
        get_latest_bar_datetime normal case
        """
        symbol = self.symbol_list[1]

        self.tested_handler.update_bars()
        result = self.tested_handler.get_latest_bar_datetime(symbol)
        expected = datetime.datetime.strptime(self.start_dates[0],
                                              "%Y-%m-%d %H:%M:%S")
        self.assertEqual(result, expected, 'error in date times, beginning')

        for dummy in range(15):
            self.tested_handler.update_bars()

        result = self.tested_handler.get_latest_bar_datetime(symbol)
        expected = datetime.datetime.strptime(self.end_dates[0],
                                              "%Y-%m-%d %H:%M:%S")
        self.assertEqual(result, expected, 'error in date times, ending')

    def test_get_latest_bar_value_not_initialized(self):
        """
        get_latest_bar_value _not_initialized
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bar_value(
                self.symbol_list[1], 'open')

    def test_get_latest_bar_value_not_symbol(self):
        """
        get_latest_bar_value not_symbol
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bar_value('ErrorSymbol', 'open')

    def test_get_latest_bar_value_not_value(self):
        """
        get_latest_bar_value not correct field value
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bar_value(self.symbol_list[0],
                                                     'ErrorField')

    def test_get_latest_bar_value_correct_value(self):
        """
        get_latest_bar_value normal case
        """
        self.tested_handler.update_bars()

        symbol = self.symbol_list[0]

        field = 'open'
        result = self.tested_handler.get_latest_bar_value(symbol, field)
        self.assertEqual(0.0, result, 'error in normal case')

        field = 'adj_close'
        result = self.tested_handler.get_latest_bar_value(symbol, field)
        self.assertEqual(0.5, result, 'error in normal case')

    def test_get_latest_bars_values_not_initialized(self):
        """
        get_latest_bars_values not_initialized
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bars_values(self.symbol_list[1],
                                                       'open', 3)

    def test_get_latest_bars_values_not_symbol(self):
        """
        get_latest_bars_values not_symbol
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bars_values('ErrorSymbol',
                                                       'high', 3)

    def test_get_latest_bars_values_not_value(self):
        """
        get_latest_bars_values not correct field value
        """
        with self.assertRaises(KeyError):
            self.tested_handler.get_latest_bars_values(self.symbol_list[0],
                                                       'ErrorField', 5)

    def test_get_latest_bars_values_correct_value(self):
        """
        get_latest_bars_values normal case
        """

        symbol = self.symbol_list[0]
        lag1 = 5
        for dummy in range(lag1):
            self.tested_handler.update_bars()

        expected = list([0.3, 1.3, 2.3, 3.3, 4.3])

        field = 'close'
        result = self.tested_handler.get_latest_bars_values(
            symbol, field, lag1)
        np.testing.assert_allclose(result, expected,
                                   err_msg='error in normal case', verbose=True)

    def test_update_bars_event_queue(self):
        """
        update_bars event in queue
        """
        self.tested_handler.update_bars()
        result = self.event.get().type
        self.assertEqual(result, 'MARKET', 'error in event queue')

    def test_update_bars_stop_iteration(self):
        """
        update_bars stop iteration
        """
        for dummy in range(16):
            self.tested_handler.update_bars()

        self.assertTrue(self.tested_handler.continue_backtest,
                        'error stop iteration')

        self.tested_handler.update_bars()

        self.assertFalse(self.tested_handler.continue_backtest,
                         'error stop iteration')
