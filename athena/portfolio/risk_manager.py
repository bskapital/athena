#!/usr/bin/python
# -*- coding: utf-8 -*-
# athena.portfolio.risk_manager.py
'''
@since: 2014-11-20
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: Handles the risk exposures for the portfolio, according
          to a predefined criteria. i.e. VaR, Kelly Criterion, etc.
'''

# TODO: all

pass
