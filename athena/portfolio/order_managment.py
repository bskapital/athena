#!/usr/bin/python
# -*- coding: utf-8 -*-
# portfolio.order_managment.py
'''
@since: 2014-11-21
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: Handles the interaction between a signal event and the
          ExecutionHandler, generating one or several orders that
          fulfill the signal.
          Different execution strategies will be here i.e Logical
          Participation Strategies as Implementation Shortfall, etc.
           

'''


pass
