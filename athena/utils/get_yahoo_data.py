"""
Helper to download data from online sources
"""

from pandas.io.data import DataReader
from datetime import datetime
import os.path
import time

def get_yahoo_data_to_csv():
    """
    get data from yahoo and save it in the indicated directory in
    individual CSV files for each symbol.
    """
    # pc = 'C:/Users/javgar119/Documents/Python/Data/'
    mac = '/Users/Javi/Documents/MarketData/'

    save_path = mac
    symbols = ['SPY', 'EWC', 'EWA', 'AAPL']
    start_date = datetime(2014, 1, 1)
    end_date = datetime(2014, 4, 1)
    pause = 0.001


    for symbol in symbols:
        try:
            full_path = os.path.join(save_path, (symbol + '-yahoofinance.csv'))
            print(full_path)
            DataReader(symbol, "yahoo", start_date, end_date).to_csv(full_path)
            time.sleep(pause)
        except:
            time.sleep(pause)


    print('Done')

if __name__ == '__main__':
    get_yahoo_data_to_csv()





