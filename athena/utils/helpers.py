"""
different helpers
"""

import pandas as pd
import numpy as np
import os


def make_fake_csv(filename, start_date, end_date,
                  style=None, save_to=None):
    """
    Function creates csv files for testing, with columns as expected
    for CSV data handler.
    Files are saved in temp directory

    Style define type of population:
        None: random numbers
        consecutive: consecutive numbers with decimal indicating column position
        zeros: zeros with decimal indicating column position
    """
    index = pd.DatetimeIndex(start=start_date, end=end_date, freq='D',
                             name='date')
    columns = ['open', 'high', 'low', 'close', 'volume', 'adj_close']

    if style == 'consecutive':
        my_data = []
        for row in range(len(index)):
            inner = []
            for col in range(6):
                inner.append(row + (col * 0.1))
            my_data.append(inner)
        data_df = pd.DataFrame(my_data, index=index, columns=columns)

    elif style == 'zeros':
        my_data = []
        for row in range(len(index)):
            inner = []
            for col in range(6):
                inner.append(0 + (col * 0.1))
            my_data.append(inner)
        data_df = pd.DataFrame(my_data, index=index, columns=columns)

    else:
        data_df = pd.DataFrame(np.random.randn(len(index), 6), index=index,
                               columns=columns)

    # save to cvs or return dataframe
    if save_to == None:
        return data_df
    else:
        data_df.to_csv(os.path.join(save_to, (filename + '.csv')))


def probando(symbol_list):
    """
    create two dataframes with contin
    """

    start_dates = ['2014-05-05 17:30:00', '2014-05-05 17:30:00']
    end_dates = ['2014-05-20 17:30:00', '2014-05-20 17:30:00']

    symbol_data = {}
    for index, symbol in enumerate(symbol_list):
        symbol_data[symbol] = make_fake_csv(symbol,
                                            start_dates[index],
                                            end_dates[index],
                                            'consecutive').iterrows()

    return symbol_data


def run_probando():
    symbol_list = ['FakeData1', 'FakeData2']
    symbol_data = probando(symbol_list)

    import time

    for symbol in symbol_list:
        for symbol_gen in symbol_data[symbol]:
            time.sleep(0.5)
            yield symbol_gen


for item in run_probando():
    print(item)
