#!/usr/bin/python
# -*- coding: utf-8 -*-
# athena.utils.simulated_series.py
'''
@since: 2014-11-22
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: 



'''

# TODO: link that look promising to develop the concept of random walk needed
# here https://github.com/wwkong/Biased-Random-Walk

# http://www.mathworks.com/help/finance/example-simulating-equity-prices.html


import numpy as np
import pandas as pd
from _datetime import datetime


def random_walk(mu, sigma, steps):
    '''
    random walk
    :param mu:
    :param sigma:
    :param steps:
    '''
    S = mu + sigma * np.random.randn(steps)
    X = S.cumsum()
    return X


def stock_random_walk(starting_price, mu, sigma, steps, name):
    '''
    Random process for stock prices.

    Geometric Brownian Motion.

    Options Futures and Other Derivatives 5th edition. John Hull.
    Pages:222-228

    :param starting_price: the stock price at the beginning of simulation
    :param mu: Expected annual return
    :param sigma: Annualized volatility
    :param steps: Forecasting horizon

    :return Pandas dataframe of simulated stock prices with datetimeindex
            starting today
    '''
    daily_sigma = sigma / np.sqrt(254)
    daily_mu = ((1 + mu) ** (1 / 254) - 1)

    prices = []
    for each_step in range(steps):
        if each_step == 0:
            price = starting_price + ((daily_mu * starting_price) +
                                      (daily_sigma * starting_price
                                       * np.random.randn()))
        else:
            price = prices[-1] + ((daily_mu * prices[-1]) +
                                  (daily_sigma * starting_price
                                   * np.random.randn()))
        prices.append(price)

    index = pd.bdate_range(
        datetime.today().strftime("%Y-%m-%d"), periods=steps, tz='UTC')

    ans = pd.DataFrame(data=prices, index=index, columns=[name])
    ans.index.names = ['Date']

    return ans

#
# class RandomMethodsFromCornell():
#     ##########################################################################
# This code belongs to:
# code form http://pages.physics.cornell.edu/~sethna/StatMech/
# ComputerExercises/PythonSoftware/RandomWalk.py
#     ##########################################################################
#
#     def RandomWalk(self, N=100, d=2):
#         """
#         Use np.cumsum and np.random.uniform to generate
#         a 2D random walk of length N, each of which has a random DeltaX and
#         DeltaY between -1/2 and 1/2.  You'll want to generate an array of
#         shape (N,d), using (for example), random.uniform(min, max, shape).
#         """
#         return np.cumsum(np.random.uniform(-0.5, 0.5, (N, d)))
#
#     def PlotRandomWalkXT(self, N=100):
#         """
#         Plot X(t) for one-dimensional random walk
#         """
#         X = self.RandomWalk(N, 1)
#         pylab.plot(X)
#         pylab.show()
#
#     def PlotRandomWalkXY(self, N=100):
#         """
#         Plot X, Y coordinates of random walk where
#             X = np.transpose(walk)[0]
#             Y = np.transpose(walk)[1]
#         To make the X and Y axes the same length,
#         use pylab.figure(figsize=(8,8)) before pylab.plot(X,Y) and
#         pylab.axis('equal') afterward.
#         """
#         walk = self.RandomWalk(N)
#         X, Y = np.transpose(walk)[0:2]
#         pylab.figure(figsize=(8, 8))
#         pylab.plot(X, Y)
#         pylab.axis('equal')
#         pylab.show()
#
#     def Endpoints(self, W=10000, N=10, d=2):
#         """
#         Returns a list of endpoints of W random walks of length N.
#         (In one dimension, this should return an array of one-element arrays,
#         to be consistent with higher dimensions.)
#         One can generate the random walks and then peel off the final positions,
#         or one can generate the steps and sum them directly, for example:
#             sum(np.random.uniform(-0.5,0.5,(10,100,2))
#         """
#         return sum(np.random.uniform(-0.5, 0.5, (N, W, d)))
#
#     def PlotEndpoints(self, W=10000, N=10, d=2):
#         """
#         Plot endpoints of random walks.
#         Use np.transpose to pull out X, Y.
#         To plot black points not joined by lines use pylab.plot(X, Y, 'k.')
#         Again, use pylab.figure(figsize=(8,8)) before and
#         pylab.axis('equal') afterward.
#         """
#         X, Y = np.transpose(self.Endpoints(W, N, d))
#         pylab.figure(figsize=(8, 8))
#         pylab.plot(X, Y, 'k.')
#         pylab.axis('equal')
#         pylab.show()
#
#     def HistogramRandomWalk(self, W=10000, N=10, d=1, bins=50):
#         """
#         Compares the histogram of random walks with the normal distribution
#         predicted by the central limit theorem.
#         #
#         (1) Plots a histogram rho(x) of the probability that a random walk
#         with N has endpoint X-coordinate at position x.
#         Uses pylab.hist(X, bins=bins, normed=1) to produce the histogram
#         #
#         (2) Calculates the RMS stepsize sigma for a random walk of length N
#         (with each step uniform in [-1/2,1/2]
#         Plots rho = (1/(sqrt(2 pi) sigma)) exp(-x**2/(2 sigma**2))
#         for -3 sigma < x < 3 sigma on the same plot (i.e., before pylab.show).
#         Hint: Create x using arange. Squaring, exponentials, and other operations
#         can be performed on whole arrays, so typing in the formula for rho will
#         work without looping over indices, except sqrt, pi, and exp need to be
#         from the appropriate library (pylab, np, ...)
#         """
#         X = self.Endpoints(W, N, d)[:, 0]
#         pylab.hist(X, bins=bins, normed=1)
#         #
#         sigma = np.sqrt(N / 12.)
#         x = np.arange(-3 * sigma, 3 * sigma, sigma / bins)
#         rho = (1 / (np.sqrt(2 * np.pi) * sigma)) * \
#             np.exp(-x ** 2 / (2 * sigma ** 2))
#         pylab.plot(x, rho, "k-")
#         pylab.show()
#
#     def yesno(self):
#         response = input('    Continue? (y/n) ')
# if len(response) == 0:        # [CR] returns true
#             return True
#         elif response[0] == 'n' or response[0] == 'N':
#             return False
# else:                       # Default
#             return True
#
#     def demo(self):
#         """
#         Demonstrates solution for exercise: example of usage
#         """
#
#         pylab.switch_backend('TkAgg')
#
#         print("Random Walk Demo")
#         print("Random Walk X vs. t")
#         self.PlotRandomWalkXT(10000)
#         if not self.yesno():
#             return
#         print("Random Walk X vs. Y")
#         self.PlotRandomWalkXY(10000)
#         if not self.yesno():
#             return
#         print("Endpoints of many random walks")
#         print("N=1: square symmetry")
#         self.PlotEndpoints(N=1)
#         if not self.yesno():
#             return
#         print("N=10: emergent circular symmetry")
#         self.PlotEndpoints(N=10)
#         if not self.yesno():
#             return
#         print("Central Limit Theorem: Histogram N=10 steps")
#         self.HistogramRandomWalk(N=10)
#         if not self.yesno():
#             return
#         print("1 step")
#         self.HistogramRandomWalk(N=1)
#         if not self.yesno():
#             return
#         print("2 steps")
#         self.HistogramRandomWalk(N=2)
#         if not self.yesno():
#             return
#         print("4 steps")
#         self.HistogramRandomWalk(N=4)
#         if not self.yesno():
#             return


# if __name__ == "__main__":
# import pylab        # Plots; also imports array functions cumsum, transpose
#     from matplotlib import pyplot as plt
#     from pprint import pprint
#     pylab.switch_backend('TkAgg')
#     mu = 0.14
#     sigma = 0.20
#     steps = 254
#
#     S = stock_random_walk(20, mu, sigma, steps, 'fake1')
#     S.plot()
#     plt.show()
