#!/usr/bin/python
# -*- coding: utf-8 -*-
# athena.data.csv_data.py
'''
@since: 2014-11-12
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: DataHandler that connects to Interactive Brokers and provides
          a market feed, both Demo or Live.
 
'''
# TODO: all
# Athena imports
from data import DataHandler
from events.events import MarketEvent

# pylint: disable=too-many-instance-attributes
# Eight is reasonable in this case.


class DemoIBDataHandler(DataHandler):

    '''


    '''
    pass


class LiveIBDataHandler(DataHandler):

    '''


    '''
    pass
