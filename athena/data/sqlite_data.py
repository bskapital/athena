#!/usr/bin/python
# -*- coding: utf-8 -*-
# athena.data.sqlite_data.py
'''
@since: 2014-11-12
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: DataHandler that reads a SQLite database from disk and provide an
          interface to obtain data in a manner identical to a market feed.
          The database schema can be found in "nmeme" folder
'''
# General imports
#import queue
import sqlite3

import sqlalchemy

from data import DataHandler
from events.events import MarketEvent
import numpy as np
import pandas as pd
# Athena imports


# pylint: disable=too-many-instance-attributes
# Ten is reasonable in this case.
class HistoricSQLiteDataHandler(DataHandler):

    """
    HistoricSQLiteDataHandler is designed to read a SQLite database for
    each requested symbol from disk and provide an interface
    to obtain the "latest" bar in a manner identical to a live
    trading interface.

    """

    def __init__(self, events, database, symbol_list):
        """
        Initialises the historic data handler by requesting
        the location of the database and a list of symbols.

        It will be assumed that all price data is in a table called
        'symbols', where the field 'symbol' is a string in the list.

        :param events: The Event Queue.
        :param database: Absolute directory path to the database
        :param symbol_list: A list of symbol strings.
        """
        self.events = events
        self.database = database
        self.symbol_list = symbol_list
        self.symbol_data = {}
        self.latest_symbol_data = {}
        self.continue_backtest = True
        self.bar_index = 0
        self.all_data_dic = {}  # access data in list form for testing

        self._open_convert_database_data()

    def _connect_to_database(self, database, flavor='sqlite3'):
        """
        Connect to the database.
        :param database: full path to SQLite3 database to connect
        """
        if flavor == 'sqlite3':
            try:
                connection = sqlite3.connect(database,
                                             detect_types=sqlite3.
                                             PARSE_DECLTYPES |
                                             sqlite3.PARSE_COLNAMES)
                return connection
            except sqlite3.Error as err:
                print('Error connecting database', err.args[0])
        # TODO:connection with SQLAlchemy needs to be properly implemented.
        elif flavor == 'SQLAlchemy':
            try:
                engine = sqlalchemy.create_engine('sqlite://' + database)
                return engine
            except sqlalchemy.exc as err:
                print('Error connecting database', err)

    def _get_prices(self, conn, symbol, cols):
        """
        Query the database and returns a dataframe with the chosen columns.
        :param conn: connection object to database.
        :param symbol: (str) symbol string as in database.
        :param cols: columns to import.
        """
        values_qry = '''SELECT {},{},{},{},{},{},{}
                        FROM prices WHERE symbol="{}"'''.format(cols[0],
                                                                cols[1],
                                                                cols[2],
                                                                cols[3],
                                                                cols[4],
                                                                cols[5],
                                                                cols[6],
                                                                symbol)

        data = pd.read_sql(values_qry, conn, index_col='date')
        return data

    def _open_convert_database_data(self):
        """
        Connect to database and query for market data for the chosen symbols.
        Return a pandas dataframe for each symbols into a dictionary.
        """
        comb_index = None

        columns = ['date',
                   'open',
                   'high',
                   'low',
                   'close',
                   'volume',
                   'adj_close']
        connection = self._connect_to_database(self.database)

        for symbol in self.symbol_list:
            self.symbol_data[symbol] = self._get_prices(
                connection, symbol, columns)

            # Combine the index to pad forward values
            if comb_index is None:
                comb_index = self.symbol_data[symbol].index
            else:
                comb_index.union(self.symbol_data[symbol].index)

            # Set the latest symbol_data to None
            self.latest_symbol_data[symbol] = []

        # Reindex the dataframes
        for symbol in self.symbol_list:
            self.all_data_dic[symbol] = self.symbol_data[symbol].\
                reindex(index=comb_index, method=None)

            self.symbol_data[symbol] = self.symbol_data[symbol].\
                reindex(index=comb_index, method=None).iterrows()

    def _get_new_bar(self, symbol):
        """
        Returns the latest bar from the data feed.
        """
        for symbol_gen in self.symbol_data[symbol]:
            yield symbol_gen

    def get_latest_bar(self, symbol):
        """
        Returns the last bar from the latest_symbol list.
        """
        print('get_latest_bar {}'.format(symbol))
        try:
            bars_list = self.latest_symbol_data[symbol]
        except KeyError:
            raise KeyError("Symbol is not available in the data set.")
        else:
            if not bars_list:
                raise KeyError('latest_symbol_data has not been initialized.')
            else:
                return bars_list[-1]

    def get_latest_bars(self, symbol, bars=1):
        """
        Returns the last N bars from the latest_symbol list,
        or N-k if less available.
        """
        try:
            bars_list = self.latest_symbol_data[symbol]
        except KeyError:
            raise KeyError("Symbol is not available in the data set.")
        else:
            if not bars_list:
                raise KeyError('latest_symbol_data has not been initialized.')
            else:
                return bars_list[-bars:]

    def get_latest_bar_datetime(self, symbol):
        """
        Returns a Python datetime object for the last bar.
        """
        try:
            bars_list = self.latest_symbol_data[symbol]
        except KeyError:
            raise KeyError("Symbol is not available in the data set.")
        else:
            if not bars_list:
                raise KeyError('latest_symbol_data has not been initialized.')
            else:
                return bars_list[-1][0]

    def get_latest_bar_value(self, symbol, val_type):
        """
        Returns one of the Open, High, Low, Close, Volume or OI
        values from the pandas Bar series object.
        """
        try:
            bars_list = self.latest_symbol_data[symbol]
        except KeyError:
            raise KeyError("Symbol is not available in the data set.")
        else:
            if not bars_list:
                raise KeyError('latest_symbol_data has not been initialized.')
            else:
                return getattr(bars_list[-1][1], val_type)

    def get_latest_bars_values(self, symbol, val_type, bars=1):
        """
        Returns the last N bar values from the
        latest_symbol list, or N-k if less available.
        """
        try:
            bars_list = self.get_latest_bars(symbol, bars)
        except KeyError:
            raise KeyError("Symbol is not available in the data set.")
        else:
            if not bars_list:
                raise KeyError('latest_symbol_data has not been initialized.')
            else:
                return np.array([getattr(b[1], val_type) for b in bars_list])

    def update_bars(self):
        """
        Pushes the latest bar to the latest_symbol_data structure
        for all symbols in the symbol list.
        """
        for symbol in self.symbol_list:
            try:
                bars = next(self._get_new_bar(symbol))
            except StopIteration:
                self.continue_backtest = False
            else:
                if bars is not None:
                    self.latest_symbol_data[symbol].append(bars)
        self.events.put(MarketEvent())

#
# if __name__ == '__main__':
#     import queue
#     WORKING_ON = 'PC'
#     if WORKING_ON == 'PC':
#         db_address = 'C:/Users/javgar119/Documents/Dropbox/SEC_MASTER/securities_master.sqlite'
#     elif WORKING_ON == 'MAC':
#         db_address = '//Users/Javier/Dropbox/SEC_MASTER/securities_master.sqlite'
#
#     LIST_OF_SYMBOLS = ['GOOG_YH']
#     EVENT = queue.Queue()
#     STORE = HistoricSQLiteDataHandler(EVENT, db_address, LIST_OF_SYMBOLS)
#
#     for item in next(STORE.update_bars('GOOG_YH')):
#         print(item)
