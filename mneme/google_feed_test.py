#!/usr/bin/python
# -*- coding: utf-8 -*-
# google_feed_test.py
'''
@since: 2014-11-28
@author: Javier Garcia
@contact: javier.garcia@bskapital.com
@summary: 

'''


import pandas.io.data as web
import pandas as pd
import datetime
from urllib.request import urlopen
import Quandl



def vendor_query(vendor, symbol, from_date, to_date):
    """
     Make a web query to data vendor
    :param vendor: 'QUANDL', 'YAHOO'
    :param symbol:
    :param from_date:
    :param to_date:
    """

    if vendor == "QUANDL":
        web_qry_result = Quandl.get(symbol,
                                    trim_start=from_date,
                                    trim_end=to_date,
                                    authtoken=QUANDL_TOKEN,
                                    verbose=False)
    elif vendor == "YAHOO":
        web_qry_result = web.get_data_yahoo(symbol,
                                            start=from_date,
                                            end=to_date)

    elif vendor == 'CSV':
          raw_read = pd.read_csv(symbol, index_col='Date', parse_dates=True)
          web_qry_result = raw_read[from_date:to_date]
    
    return web_qry_result


if __name__ == '__main__':
   
    
   QUANDL_TOKEN = "ryDzS4euF3UoFtYwswQp"
   vendor = 'QUANDL'
   symbol='CHRIS/CME_GC1'
   
   from_date= datetime.datetime(2014,11,1)
   to_date =datetime.datetime(2014,11,26)
   web_qry_result=vendor_query(vendor, symbol, from_date, to_date)
   print(web_qry_result)
   
   
   
   
   
   
   
   
   