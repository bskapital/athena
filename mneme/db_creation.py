#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 23/10/2014

@author: Javier Garcia, javier.macro.trader@gmail.com
'''

import sqlite3
import csv
# from pprint import pprint
# 'YOU/LOCATION/HERE/master_securities.SQLITE'
WORKING_ON = 'PC'


if WORKING_ON == 'PC':
    database = 'C:/Users/javgar119/Documents/Dropbox/SEC_MASTER/securities_master.sqlite'
elif WORKING_ON == 'MAC':
    database = '//Users/Javi/Dropbox/MarketDB/securities_master.sqlite'




def create_database(database):
    """

    :param location:
    """
    try:
        conn = sqlite3.connect(database)
        print("Opened database successfully")
        conn.execute('''CREATE TABLE prices (
                        idx            VARCHAR( 64 )     PRIMARY KEY,
                        symbol         VARCHAR( 64 )     NOT NULL,
                        created_date   DATETIME          NOT NULL,
                        date           DATETIME          NOT NULL,
                        open           DECIMAL( 19, 4 )  DEFAULT ( NULL ),
                        high           DECIMAL( 19, 4 )  DEFAULT ( NULL ),
                        low            DECIMAL( 19, 4 )  DEFAULT ( NULL ),
                        close          DECIMAL( 19, 4 )  NOT NULL,
                        adj_close      DECIMAL( 19, 4 )  DEFAULT ( NULL ),
                        volume         NUMERIC           DEFAULT ( NULL ),
                        open_interest  NUMERIC           DEFAULT ( NULL )
                        );''')
        print("Table 'prices' created successfully")

        conn.execute('''CREATE TABLE symbols (
                        symbol          CHAR( 32 )     PRIMARY KEY
                                                       NOT NULL,
                        currency        CHAR( 12 )     DEFAULT ( NULL ),
                        name            VARCHAR( 64 )  DEFAULT ( NULL ),
                        created_date    DATE           DEFAULT ( NULL ),
                        last_update_eod DATE           DEFAULT ( NULL ),
                        asset_type      TINYINT        DEFAULT ( NULL ),
                        vendor          CHAR( 12 )     DEFAULT ( NULL ),
                        vendor_symbol   CHAR( 64 )     DEFAULT ( NULL ),
                        tradeable       BOOLEAN        DEFAULT ( 0 ),
                        to_update       BOOLEAN        DEFAULT ( 1 )
                        );''')
        print("Table 'symbols' created successfully")
        print()
        print('Database ready to be populated')
        print()
        print('Database close')
        conn.close()
    except sqlite3.Error as err:
        print('Error', err.args[0])



def load_symbol_list_from_file(filename):
    """
    Import a csv file containing information about symbols
    into a tuple list of list.
    The structure of the csv file must be:
    Symbol, Currency, Name, Creation Date, Last update date (I use 1980-01-01
    for the first update), security type, price vendor, symbol for vendor,
    is it tradeable (1/0), update(1/0).
    -
    :param filename: location of the csv file
    """
    data = open(filename, 'r')
    try:
        ans = []

        reader = csv.reader(data)
        for row in reader:
            inner_list = []
            inner_list.append(tuple(row))
            ans.append(inner_list)
    finally:
        data.close()
    return ans


def insert_symbols(database, symbols):
    """

    """
    try:
        conn = sqlite3.connect(database, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        pointer = conn.cursor()
        for each_symbol in symbols:
            pointer.executemany("INSERT OR REPLACE INTO symbols VALUES (?,?,?,?,?,?,?,?,?,?)", each_symbol)
            conn.commit()
            print('symbol {} successfully created.'.format(each_symbol[0][0]))

        conn.close()
    except sqlite3.Error as err:
        print('Error', err.args[0])



def delete__data(database):
    """
    Clean all table data
    """
    try:
        conn = sqlite3.connect(database, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        conn.execute('DELETE FROM symbols')
        conn.commit()
        conn.execute('DELETE FROM prices')
        conn.commit()
        conn.close()
        print('Database is clean.')
    except sqlite3.Error as err:
        print('Error', err.args[0])



if __name__ == '__main__':
    delete__data(database)
    # create_database(database)
    symbols_file = 'C:/Users/javgar119/Documents/Dropbox/SEC_MASTER/symbols_list.csv'    symbols = load_symbol_list_from_file(symbols_file)
    insert_symbols(database, symbols)










